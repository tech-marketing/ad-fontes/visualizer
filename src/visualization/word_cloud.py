"""
Dashboard to create word clouds

Author William Arias

app v 1.0


"""


from wordcloud import WordCloud


def word_cloud_creator(ngrams_dataframe):
    stop_words = ['gitlab']
    text = " ".join(word for word in ngrams_dataframe.terms.astype(str))
    word_cloud = WordCloud(stopwords=stop_words, background_color='white', min_word_length=3, max_words=500, width=1000, height=500)
    word_cloud = word_cloud.generate_from_text(text)
    word_cloud.to_file('word_cloud_st.png')
    return tuple(("Word Cloud Created", "word_cloud_st.png"))