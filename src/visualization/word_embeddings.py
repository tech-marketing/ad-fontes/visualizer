"""
Dashboard to observe top terms being asked in Stackoverflow GitLab Collective

Author William Arias

app v 1.0

"""


from whatlies.transformers import Pca


def plot_embeddings(calculated_embeddings=None):
    cloud_native = ['kubernetes', 'helm', 'pod', 'container', 'gitlab']
    emb_plot = calculated_embeddings.transform(Pca(2)).assign(is_cloud=lambda e: e.name in cloud_native)
    return emb_plot.plot_interactive(color="is_cloud")


