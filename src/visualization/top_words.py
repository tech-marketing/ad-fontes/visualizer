"""

1. Import the features scripts to:
    - Run token counter over a certain column in a dataframe
    - Rank the counts of each token (Descending)
    - Create a Dataframe using the dictionary that contains the rank of counts



"""


from features.build_features import token_counter, rank_counts
import pandas as pd


def plot_rank(corpus_dataframe=None):
    """ TO-DO: add documentation """
    corpus = token_counter(corpus_dataframe)
    ranked_corpus = rank_counts(corpus)
    df_rank = pd.DataFrame.from_dict(ranked_corpus, orient='index', columns=['Frequency'])
    return  df_rank



