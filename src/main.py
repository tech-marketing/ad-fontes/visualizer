"""
Dashboard to observe top terms being asked in Stackoverflow GitLab Collective

Author William Arias

app v 1.0

"""

from features.build_features import tf_idf, embeddings
from visualization.word_cloud import word_cloud_creator
from visualization.word_embeddings import plot_embeddings
from visualization.top_words import plot_rank
from data.make_dataset import dataset_maker
import plotly.express as px
import cufflinks as cf

cf.go_offline()
import matplotlib.pyplot as plt
import streamlit as st
import streamlit.components.v1 as components
import pandas as pd


# Gets populated by the downloaded artifact in the pipeline
DATASET = 'questions/dataset-questions.csv'



def clean_df():
    df = pd.read_csv(DATASET, encoding="ISO-8859-1")
    df_questions = df.copy().drop(['Unnamed: 0'], axis=1)

    return df_questions


def tabulate_dataset():
    """ Takes the CSV dataset and converts to pandas Dataframe"""
    df = pd.read_csv(DATASET, encoding="ISO-8859-1")
    df_questions = df.copy().drop(['Unnamed: 0'], axis=1)

    return df_questions


def table_rank(ranking_data):
    """ Displays the top n-grams being asked in StackOverflow """

    ranking_table = pd.DataFrame(ranking_data, columns=['terms', 'rank'])
    ranking_table = ranking_table.sort_values('rank', ascending=False).reset_index(drop=True)

    return ranking_table


def main():
    """Executes all  functions"""
    st.set_page_config(page_title="GitLab Collective Questions Analytics", page_icon="📊")

    st.header("StackOverFlow Top questions for GitLab Collective")
    st.markdown("web application MVC - More changes to come...")
    components.html(""" <meta name="gitlab-dast-validation" content="c986f93c-4a90-432c-a5ba-f3be7af650bc">""")

    col_1, col_2 = st.columns([1, 2])

    # Brings dataset as a dataframe
    tabular_dataset_df = tabulate_dataset()
    # Selects only one column of the dataset
    so_titles = tabular_dataset_df['title_prc_bigram']
    # Reputation and Questions df
    q_rep = tabular_dataset_df[['reputation', 'title']].sort_values('reputation', ascending= False).reset_index(drop=True)

    # Passes the column with questions to TF-IDF
    ranking = tf_idf(so_titles)

    # Returns another dataframe only with Top Terms
    ranking_table = table_rank(ranking)

    with col_1:
        st.markdown(" **_Top 10_ - Topics community asked**")
        # Writes table to webapp
        st.markdown("##")
        st.markdown("##")
        st.dataframe(ranking_table.head(10).reset_index(drop=True))

    with col_2:
        st.markdown("**Most Frequent Terms**")
        top_terms_df = plot_rank(corpus_dataframe=so_titles)
        labels = dict(index="ngrams", value="Frequency")
        fig = px.bar(top_terms_df.head(5), labels=labels)
        st.plotly_chart(fig, use_container_width=True)

    # Second Section of the Dashboard

    col_3, col4 = st.columns([2,1])

    with col_3:
        st.markdown("**_Top 5_ - questions asked**")
        st.markdown("by users with High Reputation")
        st.dataframe(q_rep.head(5))
        st.markdown("by users with Intro-level Reputation")
        st.dataframe(q_rep.tail(3))

    st.subheader("Clusters of terms")

    # Converts the tabular column into a single list
    dataset = dataset_maker(so_titles)

    # Passes the dataset list to the embeddings calculator Func
    question_title_emb = embeddings(dataset)

    # Writes to front the plot
    st.write(plot_embeddings(calculated_embeddings=question_title_emb).properties(height=600, width=800))

    word_cloud = word_cloud_creator(ranking_table)
    st.image(word_cloud[1])


# Download Dataframe
    with st.sidebar:
        st.title("Export Ranking Table")
        st.download_button(
            label='Download Tokens Ranking',
            data=ranking_table.to_csv(),
            file_name="ranking_table.csv",
            mime='text/csv'
        )

if __name__ == "__main__":
    main()
